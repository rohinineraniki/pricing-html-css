let basicCost = document.getElementById("basicCost");
let professionalCost = document.getElementById("professionalCost");
let masterCost = document.getElementById("masterCost");

function toggle() {
  if (toggleButton.checked) {
    basicCost.textContent = "199.99";
    professionalCost.textContent = "249.99";
    masterCost.textContent = "399.99";
  } else {
    basicCost.textContent = "19.99";
    professionalCost.textContent = "24.99";
    masterCost.textContent = "39.99";
  }
}
